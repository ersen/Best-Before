package de.beocode.bestbefore.data

import kotlinx.collections.immutable.persistentListOf
import kotlinx.serialization.Serializable

@Serializable
data class AppData(
    val warningDays: Int = 3,
    // kapt bug: Cannot use "PersistentList<FoodItem>"
    val foodList: List<FoodItem> = persistentListOf(),
    val nameSuggestions: List<String> = persistentListOf(),
    val placeSuggestions: List<String> = persistentListOf()
)

@Serializable
data class FoodItem(
    val id: Long = System.currentTimeMillis(),
    val name: String,
    val ts: Long,
    val amount: Int,
    val place: String = ""
)