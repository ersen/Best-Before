package de.beocode.bestbefore.screens

import android.content.res.Configuration
import androidx.compose.animation.animateColorAsState
import androidx.compose.animation.core.animateFloatAsState
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.outlined.Delete
import androidx.compose.material.icons.outlined.Edit
import androidx.compose.material.icons.outlined.ShoppingBasket
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.draw.scale
import androidx.compose.ui.graphics.ColorFilter
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.input.TextFieldValue
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import androidx.navigation.NavHostController
import androidx.navigation.compose.rememberNavController
import de.beocode.bestbefore.data.FoodItem
import de.beocode.bestbefore.R
import de.beocode.bestbefore.router.Screens
import de.beocode.bestbefore.screens.general.*
import de.beocode.bestbefore.ui.theme.BestBeforeTheme
import de.beocode.bestbefore.ui.theme.Green
import de.beocode.bestbefore.ui.theme.Orange
import de.beocode.bestbefore.ui.theme.Red
import de.beocode.bestbefore.utils.convertDiffToText
import de.beocode.bestbefore.utils.getDiff
import de.beocode.bestbefore.viewmodels.interfaces.LocalUserState
import de.beocode.bestbefore.viewmodels.previews.MainViewModelPreview

@Composable
fun MainScreen(navController: NavHostController) {
    val userState = LocalUserState.current
    val snackHostState = remember { SnackbarHostState() }

    var search by remember { mutableStateOf(TextFieldValue("")) }

    TopBarMainScreen(
        settingAction = {
            navController.navigate(Screens.Settings.route)
        },
        fabAction = {
            navController.navigate(Screens.AddEdit.route)
        },
        snackHostState = snackHostState
    ) {
        if (userState.foodList.isEmpty()) {
            if (userState.isBusy) {
                Column(
                    modifier = Modifier.fillMaxSize(),
                    verticalArrangement = Arrangement.Center,
                    horizontalAlignment = Alignment.CenterHorizontally
                ) {
                    CircularProgressIndicator()
                }
            } else {
                EmptyScreen()
            }
        } else {
            Column(modifier = Modifier.fillMaxSize()) {
                SearchBar(text = search, onTextChange = {
                    search = it
                    userState.searchFoodList(search.text)
                })
                BusyIndicator(isBusy = userState.isBusy)
                LazyColumn(
                    modifier = Modifier.fillMaxSize(),
                    contentPadding = PaddingValues(horizontal = 10.dp),
                    verticalArrangement = Arrangement.Top,
                    horizontalAlignment = Alignment.CenterHorizontally
                ) {
                    items(userState.filteredFoodList, key = { it.id }) { food ->
                        FoodItem(navController, food)
                        Divider(thickness = Dp.Hairline)
                    }
                }
            }
        }
    }
}

@OptIn(ExperimentalMaterialApi::class)
@Composable
private fun FoodItem(navController: NavHostController, item: FoodItem) {
    val userState = LocalUserState.current
    val dismissState = rememberDismissState(DismissValue.Default)
    if (dismissState.isDismissed(DismissDirection.StartToEnd)) {
        LaunchedEffect(key1 = Unit) {
            navController.navigate(
                Screens.AddEdit.route + "?index=" + userState.foodList.indexOf(item)
            )
            dismissState.snapTo(DismissValue.Default)
        }
    } else if (dismissState.isDismissed(DismissDirection.EndToStart)) {
        LaunchedEffect(key1 = Unit) {
            userState.deleteFood(item)
            dismissState.snapTo(DismissValue.Default)
        }
    }

    SwipeToDismiss(
        state = dismissState,
        modifier = Modifier
            .padding(vertical = 7.5.dp)
            .clip(MaterialTheme.shapes.medium),
        dismissThresholds = {
            FractionalThreshold(0.33f)
        },
        background = {
            val color by animateColorAsState(
                when (dismissState.targetValue) {
                    DismissValue.DismissedToEnd -> Orange
                    DismissValue.DismissedToStart -> Red
                    DismissValue.Default -> MaterialTheme.colors.surface
                }
            )

            var hide = 0
            if (dismissState.targetValue == DismissValue.DismissedToEnd)
                hide = 1
            else if (dismissState.targetValue == DismissValue.DismissedToStart)
                hide = 2

            val scale by animateFloatAsState(
                if (dismissState.targetValue == DismissValue.Default) 0.75f else 1f
            )

            Row(
                Modifier
                    .fillMaxSize()
                    .background(color)
                    .padding(20.dp),
                horizontalArrangement = Arrangement.SpaceBetween
            ) {
                if (hide != 2) {
                    Icon(
                        imageVector = Icons.Outlined.Edit,
                        contentDescription = stringResource(id = R.string.edit_item),
                        modifier = Modifier
                            .fillMaxHeight()
                            .width(60.dp)
                            .scale(scale),
                        tint = MaterialTheme.colors.background
                    )
                }
                Spacer(modifier = Modifier.width(5.dp))
                if (hide != 1) {
                    Icon(
                        imageVector = Icons.Outlined.Delete,
                        contentDescription = stringResource(id = R.string.delete_item),
                        modifier = Modifier
                            .fillMaxHeight()
                            .width(60.dp)
                            .scale(scale),
                        tint = MaterialTheme.colors.background
                    )
                }
            }
        }) {
        FoodCard(item)
    }
}

@Composable
private fun FoodCard(item: FoodItem) {
    val context = LocalContext.current
    Card(
        modifier = Modifier.fillMaxWidth(),
        elevation = 5.dp
    ) {
        Column(modifier = Modifier.padding(10.dp)) {
            Text(
                text = item.name,
                fontWeight = FontWeight.Bold,
                overflow = TextOverflow.Ellipsis,
                maxLines = 1,
                style = MaterialTheme.typography.h2
            )
            ExpireText(item.ts)
            Text(
                text = context.resources.getQuantityString(
                    R.plurals.amount,
                    item.amount,
                    item.amount
                ),
                overflow = TextOverflow.Ellipsis,
                maxLines = 1
            )
            if (item.place != "")
                Text(
                    text = context.getString(R.string.place, item.place),
                    overflow = TextOverflow.Ellipsis,
                    maxLines = 1,
                    style = MaterialTheme.typography.body2
                )
        }
    }
}

@Composable
private fun ExpireText(ts: Long) {
    val userState = LocalUserState.current
    val context = LocalContext.current

    val diff = getDiff(ts)
    val color = if (diff < 0) {
        Red
    } else if (diff <= userState.expireWarning) {
        Orange
    } else {
        Green
    }
    Text(
        text = convertDiffToText(context, diff),
        color = color,
        overflow = TextOverflow.Ellipsis,
        maxLines = 2,
        style = MaterialTheme.typography.h5
    )
}

@Composable
private fun EmptyScreen() {
    val context = LocalContext.current
    Column(
        modifier = Modifier
            .fillMaxSize()
            .padding(10.dp),
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Image(
            imageVector = Icons.Outlined.ShoppingBasket,
            contentDescription = null,
            modifier = Modifier
                .fillMaxHeight(0.25f)
                .fillMaxWidth(),
            colorFilter = ColorFilter.tint(MaterialTheme.colors.primary)
        )
        Text(
            text = context.getString(R.string.no_data),
            modifier = Modifier.padding(vertical = 10.dp),
            fontWeight = FontWeight.Bold,
            style = MaterialTheme.typography.h1
        )
        Text(
            text = context.getString(R.string.no_data_add),
            textAlign = TextAlign.Center,
            style = MaterialTheme.typography.h2
        )
        Spacer(modifier = Modifier.fillMaxHeight(0.25f))
    }
}

@Preview(name = "LightMode", showBackground = true)
@Preview(name = "DarkMode", showBackground = true, uiMode = Configuration.UI_MODE_NIGHT_YES)
@Composable
private fun DefaultPreview() {
    val navController = rememberNavController()
    BestBeforeTheme {
        CompositionLocalProvider(LocalUserState provides MainViewModelPreview()) {
            MainScreen(navController = navController)
        }
    }
}