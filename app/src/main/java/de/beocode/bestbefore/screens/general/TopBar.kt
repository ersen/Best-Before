package de.beocode.bestbefore.screens.general

import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Add
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.material.icons.outlined.Save
import androidx.compose.material.icons.outlined.Settings
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.alpha
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import de.beocode.bestbefore.R

@Composable
fun TopBarMainScreen(
    settingAction: () -> Unit,
    fabAction: () -> Unit,
    snackHostState: SnackbarHostState,
    content: @Composable () -> Unit
) {
    val context = LocalContext.current
    Scaffold(
        scaffoldState = rememberScaffoldState(snackbarHostState = snackHostState),
        topBar = {
            TopAppBar(backgroundColor = MaterialTheme.colors.background, elevation = 0.dp) {
                Box(modifier = Modifier.fillMaxSize()) {
                    Text(
                        text = context.getString(R.string.app_name),
                        modifier = Modifier.align(Alignment.Center),
                        color = MaterialTheme.colors.onBackground,
                        fontWeight = FontWeight.Bold,
                        textAlign = TextAlign.Center,
                        style = MaterialTheme.typography.h3
                    )
                    IconButton(
                        onClick = settingAction,
                        modifier = Modifier.align(Alignment.CenterEnd)
                    ) {
                        Icon(
                            imageVector = Icons.Outlined.Settings,
                            contentDescription = context.getString(R.string.settings),
                            tint = MaterialTheme.colors.onBackground
                        )
                    }
                }
            }
        },
        snackbarHost = {
            SnackbarHost(it) { data ->
                Snackbar(
                    snackbarData = data,
                    backgroundColor = MaterialTheme.colors.surface,
                    contentColor = MaterialTheme.colors.onSurface,
                    actionColor = MaterialTheme.colors.primary
                )
            }
        },
        floatingActionButton = {
            ExtendedFloatingActionButton(
                text = {
                    Text(text = context.getString(R.string.add_item))
                },
                onClick = fabAction,
                icon = {
                    Icon(imageVector = Icons.Filled.Add, contentDescription = null)
                }
            )
        },
        backgroundColor = MaterialTheme.colors.background
    ) {
        content()
    }
}

@Composable
fun TopBarBackSafe(
    title: String,
    isBusy: Boolean,
    backAction: () -> Unit,
    saveAction: (() -> Unit)?,
    snackHostState: SnackbarHostState,
    content: @Composable () -> Unit
) {
    val context = LocalContext.current
    Scaffold(
        scaffoldState = rememberScaffoldState(snackbarHostState = snackHostState),
        topBar = {
            TopAppBar(backgroundColor = MaterialTheme.colors.background, elevation = 0.dp) {
                Row(
                    modifier = Modifier.fillMaxSize(),
                    horizontalArrangement = Arrangement.SpaceBetween,
                    verticalAlignment = Alignment.CenterVertically
                ) {
                    IconButton(onClick = backAction) {
                        Icon(
                            imageVector = Icons.Filled.ArrowBack,
                            contentDescription = context.getString(R.string.label_back),
                            tint = MaterialTheme.colors.onBackground
                        )
                    }
                    Text(
                        text = title,
                        color = MaterialTheme.colors.onBackground,
                        fontWeight = FontWeight.Bold,
                        style = MaterialTheme.typography.h3
                    )
                    if (saveAction != null) {
                        IconButton(onClick = saveAction, enabled = !isBusy) {
                            Icon(
                                imageVector = Icons.Outlined.Save,
                                contentDescription = context.getString(R.string.label_save),
                                modifier = Modifier.alpha(if (isBusy) 0.5f else 1f),
                                tint = MaterialTheme.colors.onBackground,
                            )
                        }
                    } else {
                        IconButton(onClick = { }, enabled = false) {
                        }
                    }
                }
            }
        },
        snackbarHost = {
            SnackbarHost(it) { data ->
                Snackbar(
                    snackbarData = data,
                    backgroundColor = MaterialTheme.colors.surface,
                    contentColor = MaterialTheme.colors.onSurface,
                    actionColor = MaterialTheme.colors.primary
                )
            }
        },
        backgroundColor = MaterialTheme.colors.background
    ) {
        content()
    }
}