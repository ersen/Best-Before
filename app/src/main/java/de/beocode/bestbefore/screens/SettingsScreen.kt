package de.beocode.bestbefore.screens

import android.content.Context
import android.content.Intent
import android.content.res.Configuration
import android.net.Uri
import android.os.Build
import androidx.activity.compose.rememberLauncherForActivityResult
import androidx.activity.result.contract.ActivityResultContracts
import androidx.compose.foundation.Image
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.selection.selectable
import androidx.compose.foundation.selection.selectableGroup
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowForwardIos
import androidx.compose.material.icons.outlined.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.ColorFilter
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.semantics.Role
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.input.TextFieldValue
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavHostController
import androidx.navigation.compose.rememberNavController
import de.beocode.bestbefore.R
import de.beocode.bestbefore.data.Theme
import de.beocode.bestbefore.screens.general.TopBarBackSafe
import de.beocode.bestbefore.ui.theme.BestBeforeTheme
import de.beocode.bestbefore.utils.CreateDocument
import de.beocode.bestbefore.viewmodels.interfaces.LocalUserState
import de.beocode.bestbefore.viewmodels.interfaces.MainViewModelInterface
import de.beocode.bestbefore.viewmodels.previews.MainViewModelPreview
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch

private enum class Cards {
    AppSettings,
    ImportExport,
    Information
}

private data class DialogState(
    var showDialog: Boolean,
    var title: String = "",
    var body: @Composable () -> Unit = { },
    var posClick: () -> Unit = { }
)

@Composable
fun SettingsScreen(
    navController: NavHostController,
    scope: CoroutineScope
) {
    val context = LocalContext.current
    val userState = LocalUserState.current
    val snackHostState = remember { SnackbarHostState() }
    var dialogState by remember {
        mutableStateOf(DialogState(showDialog = false))
    }

    TopBarBackSafe(
        title = context.getString(R.string.settings),
        isBusy = false,
        backAction = { navController.popBackStack() },
        saveAction = null,
        snackHostState = snackHostState
    ) {
        LazyColumn(
            modifier = Modifier
                .fillMaxSize(),
            contentPadding = PaddingValues(horizontal = 15.dp),
            verticalArrangement = Arrangement.Top,
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            item { Card(Cards.AppSettings, scope, snackHostState) { dialogState = it } }
            item { Card(Cards.ImportExport, scope, snackHostState) { dialogState = it } }
            item { Card(Cards.Information, scope, snackHostState) { dialogState = it } }
            item { Footer(scope, snackHostState, userState) { dialogState = it } }
        }
        if (dialogState.showDialog) {
            ShowDialog(dialogState) { dialogState = dialogState.copy(showDialog = false) }
        }
    }
}

@Composable
private fun Card(
    type: Cards,
    scope: CoroutineScope,
    snackHostState: SnackbarHostState,
    dialogChange: (DialogState) -> Unit
) {
    Card(
        modifier = Modifier
            .fillMaxWidth()
            .padding(vertical = 10.dp),
        elevation = 5.dp
    ) {
        when (type) {
            Cards.AppSettings -> AppSettings(scope, snackHostState, dialogChange)
            Cards.ImportExport -> ImportExport(scope, snackHostState, dialogChange)
            Cards.Information -> Information()
        }
    }
}

@Composable
private fun AppSettings(
    scope: CoroutineScope,
    snackHostState: SnackbarHostState,
    dialogChange: (DialogState) -> Unit
) {
    val context = LocalContext.current
    val userState = LocalUserState.current

    Column(modifier = Modifier.padding(15.dp)) {
        Text(
            text = context.getString(R.string.app_settings),
            modifier = Modifier.padding(bottom = 5.dp),
            fontWeight = FontWeight.Bold,
            style = MaterialTheme.typography.h4
        )
        var warningDays by remember { mutableStateOf(TextFieldValue("")) }
        CardListItem(
            image = Icons.Outlined.EditCalendar,
            text = context.getString(R.string.change_warning_days)
        ) {
            warningDays = warningDays.copy(userState.expireWarning.toString())
            dialogChange(DialogState(true, context.getString(R.string.change_warning_days), {
                OutlinedTextField(
                    value = warningDays,
                    onValueChange = { warningDays = it },
                    modifier = Modifier
                        .fillMaxWidth(),
                    label = { Text(text = context.getString(R.string.label_days)) },
                    keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Number),
                    singleLine = true
                )
            }) {
                val days = warningDays.text.toIntOrNull()
                if (days != null && days >= 0) {
                    userState.setWarningDays(days)
                } else {
                    scope.launch {
                        snackHostState.showSnackbar(
                            message = context.getString(R.string.error_only_digits),
                            duration = SnackbarDuration.Short
                        )
                    }
                }
            })
        }
        var appTheme by remember { mutableStateOf(userState.appTheme) }
        CardListItem(
            image = Icons.Outlined.Palette,
            text = stringResource(id = R.string.change_theme)
        ) {
            appTheme = userState.appTheme
            dialogChange(DialogState(true, context.getString(R.string.change_theme), {
                Column(Modifier.selectableGroup()) {
                    Theme.values().forEach { theme ->
                        Row(
                            Modifier
                                .fillMaxWidth()
                                .padding(2.5.dp)
                                .clip(RoundedCornerShape(5.dp))
                                .selectable(
                                    selected = (theme == appTheme),
                                    onClick = { appTheme = theme },
                                    role = Role.RadioButton
                                )
                                .padding(2.5.dp),
                            horizontalArrangement = Arrangement.spacedBy(16.dp),
                            verticalAlignment = Alignment.CenterVertically
                        ) {
                            RadioButton(
                                selected = (theme == appTheme),
                                onClick = null
                            )
                            Text(text = stringResource(id = theme.keyName))
                        }
                    }
                }
            }) {
                userState.setTheme(appTheme)
            })
        }
    }
}

@Composable
private fun ImportExport(
    scope: CoroutineScope,
    snackHostState: SnackbarHostState,
    dialogChange: (DialogState) -> Unit
) {
    val context = LocalContext.current
    val userState = LocalUserState.current

    val openFile = rememberLauncherForActivityResult(
        contract = ActivityResultContracts.OpenDocument()
    ) { uri ->
        scope.launch { userState.importData(uri) {
            scope.launch {
                snackHostState.showSnackbar(
                    message = context.getString(
                        if (it) R.string.import_success else R.string.import_failed
                    ),
                    duration = SnackbarDuration.Short
                )
            }
        } }
    }

    val createFile = rememberLauncherForActivityResult(
        contract = CreateDocument("application/json")
    ) { uri ->
        scope.launch {
            userState.exportData(uri) {
                scope.launch {
                    snackHostState.showSnackbar(
                        message = context.getString(
                            if (it) R.string.export_success else R.string.export_failed
                        ),
                        duration = SnackbarDuration.Short
                    )
                }
            }
        }
    }

    Column(modifier = Modifier.padding(15.dp)) {
        Text(
            text = context.getString(R.string.app_settings),
            modifier = Modifier.padding(bottom = 5.dp),
            fontWeight = FontWeight.Bold,
            style = MaterialTheme.typography.h4
        )
        CardListItem(
            image = Icons.Outlined.FileDownload,
            text = context.getString(R.string.import_data)
        ) {
            dialogChange(DialogState(true, context.getString(R.string.attention), {
                Text(
                    text = context.getString(R.string.import_warning),
                    style = MaterialTheme.typography.body2
                )
            }) {
                openFile.launch(arrayOf("application/json"))
            })
        }
        CardListItem(
            image = Icons.Outlined.FileUpload,
            text = context.getString(R.string.export_data)
        ) {
            createFile.launch("BestBefore-Backup.json")
        }
    }
}

@Composable
private fun Information() {
    val context = LocalContext.current
    Column(modifier = Modifier.padding(15.dp)) {
        Text(
            text = context.getString(R.string.information),
            modifier = Modifier.padding(bottom = 5.dp),
            fontWeight = FontWeight.Bold,
            style = MaterialTheme.typography.h4
        )
        CardListItem(
            image = Icons.Outlined.Code,
            text = context.getString(R.string.source_code)
        ) {
            openWebsite(context, context.getString(R.string.repo_url))
        }
        CardListItem(
            image = Icons.Outlined.BugReport,
            text = context.getString(R.string.create_ticket)
        ) {
            openWebsite(context, context.getString(R.string.repo_url) + "/issues")
        }
        CardListItem(
            image = Icons.Outlined.Email,
            text = context.getString(R.string.send_email)
        ) {
            sendEmail(context)
        }
    }
}

@Composable
private fun CardListItem(image: ImageVector, text: String, action: () -> Unit) {
    Row(
        modifier = Modifier
            .fillMaxWidth()
            .clip(RoundedCornerShape(5.dp))
            .clickable(onClick = action)
            .padding(horizontal = 5.dp, vertical = 10.dp),
        verticalAlignment = Alignment.CenterVertically
    ) {
        Image(
            imageVector = image,
            contentDescription = text,
            modifier = Modifier
                .size(28.dp),
            colorFilter = ColorFilter.tint(color = MaterialTheme.colors.primary)
        )
        Text(text = text, modifier = Modifier.padding(horizontal = 5.dp))
        Image(
            imageVector = Icons.Filled.ArrowForwardIos,
            contentDescription = null,
            modifier = Modifier
                .fillMaxWidth()
                .size(18.dp),
            alignment = Alignment.CenterEnd,
            colorFilter = ColorFilter.tint(MaterialTheme.colors.onSurface)
        )
    }
}

@Composable
private fun Footer(
    scope: CoroutineScope,
    snackHostState: SnackbarHostState,
    vm: MainViewModelInterface,
    dialogChange: (DialogState) -> Unit
) {
    val context = LocalContext.current
    Column(
        modifier = Modifier.padding(vertical = 5.dp),
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        OutlinedButton(
            onClick = {
                dialogChange(DialogState(true, context.getString(R.string.delete_data), {
                    Text(
                        text = context.getString(R.string.rly_delete_all_data),
                        style = MaterialTheme.typography.body2
                    )
                }) {
                    scope.launch {
                        vm.clearData {
                            val body =
                                context.getString(if (it) R.string.removed_all_data else R.string.error_occurred)
                            scope.launch {
                                snackHostState.showSnackbar(
                                    message = body,
                                    duration = SnackbarDuration.Short
                                )
                            }
                        }
                    }
                })
            },
            enabled = !vm.isBusy,
            elevation = ButtonDefaults.elevation(
                defaultElevation = 5.dp,
                pressedElevation = 3.dp
            ),
            shape = CircleShape
        ) {
            if (vm.isBusy) {
                CircularProgressIndicator()
            } else {
                Text(
                    text = context.getString(R.string.delete_data),
                    modifier = Modifier.padding(horizontal = 50.dp, vertical = 0.dp),
                    color = MaterialTheme.colors.primary,
                    fontSize = 20.sp
                )
            }
        }
        Text(
            text = context.getString(
                R.string.version,
                context.getString(R.string.app_version),
                context.getString(R.string.app_code)
            ),
            modifier = Modifier.padding(vertical = 10.dp),
            style = MaterialTheme.typography.caption
        )
    }
}

@Composable
private fun ShowDialog(dialogState: DialogState, onDismiss: () -> Unit) {
    val context = LocalContext.current
    AlertDialog(
        title = { Text(text = dialogState.title, style = MaterialTheme.typography.h5) },
        text = dialogState.body,
        dismissButton = {
            OutlinedButton(
                onClick = {
                    onDismiss()
                },
                modifier = Modifier.padding(8.dp)
            ) {
                Text(
                    text = context.getString(R.string.label_cancel),
                    color = MaterialTheme.colors.primary
                )
            }
        },
        confirmButton = {
            Button(
                onClick = {
                    dialogState.posClick()
                    onDismiss()
                },
                modifier = Modifier.padding(8.dp)
            ) {
                Text(text = context.getString(R.string.label_continue))
            }
        },
        onDismissRequest = onDismiss
    )
}

private fun openWebsite(context: Context, link: String) {
    context.startActivity(
        Intent(
            Intent.ACTION_VIEW,
            Uri.parse(link)
        )
    )
}

private fun sendEmail(context: Context) {
    val intent = Intent(Intent.ACTION_SEND).apply {
        type = "text/email"
        putExtra(Intent.EXTRA_TITLE, context.getString(R.string.choose_email_app))
        putExtra(Intent.EXTRA_EMAIL, arrayOf("best-before@beocode.eu"))
        putExtra(
            Intent.EXTRA_TEXT,
            context.getString(
                R.string.feedback_body,
                Build.VERSION.RELEASE,
                context.getString(R.string.app_version)
            )
        )
    }
    context.startActivity(Intent.createChooser(intent, null))
}

@Preview(name = "LightMode", showBackground = true)
@Preview(name = "DarkMode", showBackground = true, uiMode = Configuration.UI_MODE_NIGHT_YES)
@Composable
private fun DefaultPreview() {
    val navController = rememberNavController()
    val scope = rememberCoroutineScope()
    BestBeforeTheme {
        CompositionLocalProvider(LocalUserState provides MainViewModelPreview()) {
            SettingsScreen(navController = navController, scope = scope)
        }
    }
}