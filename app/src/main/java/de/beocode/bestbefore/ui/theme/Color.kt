package de.beocode.bestbefore.ui.theme

import androidx.compose.ui.graphics.Color

val White = Color(0xFFFEFEFE)
val LightGray = Color(0xFFE4E4E7)
val DarkGray1 = Color(0xFF666666)
val DarkGray2 = Color(0xFF202020)
val DarkGray3 = Color(0xFF101010)
val Green = Color(0xFF388E3C)
val Orange = Color(0xFFF57C00)
val Red = Color(0xFFD32F2F)