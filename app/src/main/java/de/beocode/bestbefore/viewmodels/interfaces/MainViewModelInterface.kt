package de.beocode.bestbefore.viewmodels.interfaces

import android.net.Uri
import androidx.compose.runtime.compositionLocalOf
import de.beocode.bestbefore.data.FoodItem
import de.beocode.bestbefore.data.Theme
import kotlinx.collections.immutable.PersistentList

interface MainViewModelInterface {

    var isBusy: Boolean

    var appTheme: Theme
    var expireWarning: Int
    var foodList: PersistentList<FoodItem>
    var filteredFoodList: List<FoodItem>
    var nameSuggestions: PersistentList<String>
    var placeSuggestions: PersistentList<String>

    fun searchFoodList(search: String)

    fun addFood(foodItem: FoodItem, success: (Boolean) -> Unit)
    fun updateFood(index: Int, foodItem: FoodItem, success: (Boolean) -> Unit)
    fun deleteFood(foodItem: FoodItem)

    fun addSuggestions(name: String, destination: String)

    fun setWarningDays(days: Int)

    fun setTheme(theme: Theme)

    suspend fun importData(uri: Uri?, success: (Boolean) -> Unit)
    suspend fun exportData(uri: Uri?, success: (Boolean) -> Unit)

    suspend fun clearData(success: (Boolean) -> Unit)
}

val LocalUserState = compositionLocalOf<MainViewModelInterface> { error("User State Context not found!") }