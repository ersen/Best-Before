package de.beocode.bestbefore.utils

import java.util.*

fun getDiff(ts: Long): Int {
    val cal = Calendar.getInstance(Locale.getDefault())
    cal.timeInMillis = System.currentTimeMillis()
    cal.set(Calendar.HOUR_OF_DAY, 0)
    cal.set(Calendar.MINUTE, 0)

    return ((ts - cal.timeInMillis) / 86400000).toInt()
}