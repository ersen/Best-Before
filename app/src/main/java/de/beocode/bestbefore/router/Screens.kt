package de.beocode.bestbefore.router

sealed class Screens(val route: String, val arguments: String = "") {
    object Main : Screens(route = "main_screen")
    object AddEdit : Screens(route = "add_edit_screen", "?index={index}")
    object Settings : Screens(route = "settings_screen")
}