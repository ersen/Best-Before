package de.beocode.bestbefore.app

import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.runtime.Composable
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Modifier
import de.beocode.bestbefore.router.BestBeforeRouter

@Composable
fun BestBeforeApp() {
    Surface(modifier = Modifier.fillMaxSize(), color = MaterialTheme.colors.background) {
        BestBeforeRouter(scope = rememberCoroutineScope())
    }
}