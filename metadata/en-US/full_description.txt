Features:
- add, edit and remove products
- save best-before date
- search through products
- data import and export

Supported languages:
- English
- Czech
- Danish
- Dutch
- Finnish
- French
- German
- Hungarian
- Polish
- Portuguese (BR)
- Spanish (AR)
- Ukrainian

Wiki:
You have questions, need help to set up the app or simply want to know more about it? Then take a look at the <a href="https://codeberg.org/BeoCode/Best-Before/wiki">Wiki</a>.

Licensing:
Licensed under the <a href="https://codeberg.org/BeoCode/Best-Before/src/branch/main/LICENSE">EUPL-1.2</a>.

OpenSource:
This app is <a href="https://codeberg.org/BeoCode/Best-Before">open source</a>.
Many thanks to all <a href="https://codeberg.org/BeoCode/Best-Before/wiki/Contributors">contributors</a>.

Icons:
<a href="https://fonts.google.com/icons?icon.set=Material+Icons">Icons</a> by Google licensed under <a href="https://www.apache.org/licenses/LICENSE-2.0.html">Apache License 2.0</a>.
