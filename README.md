[![status-badge](https://ci.codeberg.org/api/badges/BeoCode/Best-Before/status.svg)](https://ci.codeberg.org/BeoCode/Best-Before)
[![license](https://img.shields.io/badge/license-EUPL--1.2-blue)](https://codeberg.org/BeoCode/Best-Before/src/branch/main/LICENSE)
[![F-Droid version](https://img.shields.io/f-droid/v/de.beocode.bestbefore)](https://f-droid.org/de/packages/de.beocode.bestbefore/)
[![Download F-Droid](https://img.shields.io/badge/download-F--Droid-orange)](https://f-droid.org/de/packages/de.beocode.bestbefore/)
[![Download PlayStore](https://img.shields.io/badge/download-PlayStore-green)](https://play.google.com/store/apps/details?id=de.beocode.bestbefore)
[![Translation](https://img.shields.io/badge/translation-POEditor-pink)](https://poeditor.com/join/project/sYYYC34Bgf)

# Best-Before

<img src="https://codeberg.org/BeoCode/Best-Before/raw/branch/main/app/src/main/ic_launcher-playstore.png" height=75px> **App for monitoring the best-before date of products**

[<img src="https://fdroid.gitlab.io/artwork/badge/get-it-on.png"
alt="Get it on F-Droid"
height="60">](https://f-droid.org/de/packages/de.beocode.bestbefore/)
[<img src="https://play.google.com/intl/en_us/badges/images/generic/en-play-badge.png"
alt="Get it on Google Play"
height="60">](https://play.google.com/store/apps/details?id=de.beocode.bestbefore)

## Features
- add, edit and remove products
- save best-before date
- search through products
- data import and export

## Supported languages:
- English
- Czech
- Danish
- Dutch
- Finnish
- French
- German
- Hungarian
- Polish
- Portuguese (BR)
- Spanish (AR)
- Ukrainian

## Icons
[Icons](https://fonts.google.com/icons?icon.set=Material+Icons) by Google licensed under [Apache License 2.0](https://www.apache.org/licenses/LICENSE-2.0.html).

## Contributing
See our [Contributing doc](CONTRIBUTING.md) for information on how to report issues or translate the app into your language.  
Many thanks to all [contributors](https://codeberg.org/BeoCode/Best-Before/wiki/Contributors).

## Wiki
You have questions, need help to set up the app or simply want to know more about it? Then take a look at the [Wiki](https://codeberg.org/BeoCode/Best-Before/wiki).

## Licensing
See the [LICENSE](LICENSE) file for licensing information as it pertains to files in this repository.
